package ru.nsu.fit.nikiforov;

import lombok.SneakyThrows;

import java.util.List;

public class Main {

    private static final String FILENAME = "src/main/resources/people.xml";

    @SneakyThrows
    public static void main(String[] args) {
        int threadsCount = Runtime.getRuntime().availableProcessors();

        List<Integer> segmentIndexes = new XmlFileSegmentsDetector(
            threadsCount, FILENAME, "person").getSegmentIndexes();

        new ConcurrentXmlToDataBaseUploader(FILENAME, segmentIndexes).upload();
    }
}
