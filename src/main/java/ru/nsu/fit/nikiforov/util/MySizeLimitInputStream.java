package ru.nsu.fit.nikiforov.util;

import com.Ostermiller.util.SizeLimitInputStream;

import java.io.IOException;
import java.io.InputStream;

public class MySizeLimitInputStream extends SizeLimitInputStream {

    public MySizeLimitInputStream(InputStream in, long maxBytesToRead){
        super(in, maxBytesToRead);
    }

    @Override
    public long skip(long n) throws IOException {
        long skippedBytes = super.skip(n);
        if (skippedBytes < 0) {
            skippedBytes = 0;
        }
        bytesRead += skippedBytes;
        return skippedBytes;
    }
}
